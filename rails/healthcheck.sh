#!/bin/bash

var=`nc -z localhost 3000; echo $?`;
if [ $var -eq 0 ]
then
    echo "port 3000 is available" > /dev/null 2>&1
else
    echo "port 3000 is not available" > /dev/null 2>&1
fi

